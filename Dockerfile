FROM d4science/gcore-distribution
 ENV START_SCOPE=devNext
 ENV INFRASTRUCTURE=gcube
 ENV COMPLETE_START_SCOPE=/gcube/devNext
 ENV GCORE_HOST=localhost
 RUN wget --no-check-certificate https://nexus.d4science.org/nexus/content/repositories/gcube-staging-gcore/org/gcube/informationsystem/is-registry-service/2.1.4-4.16.0-126945/is-registry-service-2.1.4-4.16.0-126945.gar && mv is-registry-service-2.1.4-4.16.0-126945.gar is-registry-service.gar && gcore-deploy-service is-registry-service.gar
 COPY src/GHNConfig.xml /home/gcube/gCore/config/GHNConfig.xml
 COPY src/jndi-config.xml /home/gcube/gCore/etc/is-registry-service/jndi-config.xml
 COPY src/startService.sh /home/gcube/startService.sh
 RUN chmod 755 /home/gcube/startService.sh
 ENTRYPOINT exec /home/gcube/startService.sh