sed -i "s#{{STARTSCOPE}}#$START_SCOPE#g; s#{{INFRASTRUCTURE}}#$INFRASTRUCTURE#g"  /home/gcube/gCore/config/GHNConfig.xml
sed -i "s#{{COMPLETESTARTSCOPE}}#$COMPLETE_START_SCOPE#g" /home/gcube/gCore/etc/is-registry-service/jndi-config.xml
sed -i "s#<parameter name=\"logicalHost\" value=\"localhost\"/>#<parameter name=\"logicalHost\" value=\"$GCORE_HOST\"/>#g" /home/gcube/gCore/etc/globus_wsrf_core/server-config.wsdd
exec /home/gcube/gCore/bin/gcore-start-container