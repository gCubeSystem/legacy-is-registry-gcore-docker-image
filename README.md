# Legacy IS Registry gCore based

An image with the legacy gCube IS-Registry

## Build the image

```shell
$ docker build -t d4science/legacy-is-registry-gcore .
```

## Run the image

The following Environment variable are needed to be set:
* **START_SCOPE** default is devNext
* **INFRASTRUCTURE** default is gcube
* **COMPLETE_START_SCOPE** default is /gcube/devNext
* **GCORE_HOST** default is localhost

```shell
$ docker container run d4science/legacy-is-registry-gcore:latest  --env-file ./registry.config --name is-registry
```
