# Tips, links to the documentation

## Official documentation

<https://docs.docker.com>
<https://docs.docker.com/get-started/>
<https://docs.docker.com/develop/>
<https://docs.docker.com/develop/develop-images/dockerfile_best-practices/>

Some hints on how to debug a docker image: <http://www.openwebit.com/c/how-to-debug-docker-images/>
